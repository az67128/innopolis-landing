export const BASE_URL = (() => {
  if (location.href.includes("gitlab"))
    return "https://az67128.gitlab.io/7-sins";
  if (location.href.includes("localhost") || location.href.includes("192.168"))
    return ".";
  return "https://az67128.gitlab.io/7-sins";
})();

export function isIos() {
    return (
      [
        "iPad Simulator",
        "iPhone Simulator",
        "iPod Simulator",
        "iPad",
        "iPhone",
        "iPod",
      ].includes(navigator.platform) ||
      // iPad on iOS 13 detection
      (navigator.userAgent.includes("Mac") && "ontouchend" in document)
    );
  }

  export function unlockScroll () {
    const scrollY = document.body.style.top;
    document.body.style.position = '';
    document.body.style.top = '';
    document.body.style.left = '';
    document.body.style.right = '';
    window.scrollTo(0, parseInt(scrollY || '0') * -1);
};

export function lockScroll () {
    document.body.style.position = 'fixed';
    document.body.style.top = `-${window.scrollY}px`;
    document.body.style.left = '0';
    document.body.style.right = '0';
};