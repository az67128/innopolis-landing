export default [
  
  {
    number: 'Фильм 1',
    alias: 'ifThen',
    image: 'series1.jpg',
    name: 'Если [то]',
    duration: '9 мин',
    description:
      'Разработчик Саша влюблен в менеджера Вику, с которой работает в одном офисе. Саша уже год работает над “волшебным кодом”, которые должен решать все жизненные проблемы. Друг предлагает Саше протестировать код на себе и с его помощью завоевать сердце Вики.',
    // fullDescription: ``,
    url: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/85cf0123-68dc-4768-9eda-270fb478606f/eslito.ism/manifest',
    // trailer: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    // trailerSmall: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    authors: 'Автор сценария и продюсер: <b>Дарья Баженова</b><br/>Режиссер: <b>Полина Окунева</b><br/>Оператор: <b>Кирилл Чернега</b><br/>Актеры: <b>Дарья Косова,Никита Калинин, Камиль Юсипов, монтажер: Кирилл Чернега<b><br/>',
    sequence: [
      {
        start: 476.619257,
        end: 513,
        type: 'NextSeries',
      },
    ],
  },
  // {
  //   number: 'Фильм 2',
  //   alias: 'Trainees',
  //   image: 'series2.jpg',
  //   name: 'Стажеры',
  //   duration: '13 мин',
  //   description:
  //     'Двое новеньких стажеров теряют в технопарке робота, полного коммерческих тайн. Теперь, чтобы не вылететь с работы, им нужно найти его быстрее, чем придет начальник.',
  //   // fullDescription: ``,
  //   url: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/3ddcc208-19ca-4a58-a999-9526611407db/stageri.ism/manifest',
  //   // trailer: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
  //   // trailerSmall: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
  //   authors: 'Режиссер-постановщик: <b>Максим Столбов</b><br/>Оператор-постановщик: <b>Никита Бондаренко</b><br/>Авторы сценария: <b>Михаил Силин, Анастасия Раймова, Максим Столбов</b><br/>Продюсеры: <b>Михаил Силин, Максим Столбов</b><br/>Актеры: <b>Игорь Харитонов, Зинаида Ястржембская, Вячеслав Волков</b><br/>Монтаж: <b>Максим Столбов, Арина Пушнякова</b><br/>Цветокоррекция: <b>Никита Бондаренко</b><br/>Композитор: <b>Александр Галкин</b><br/>Сведение и саунд-дизайн: <b>Амаль Ковтун</b>',
  //   sequence: [
  //     {
  //       start: 695.726099,
  //       end: 796,
  //       type: 'NextSeries',
  //     },
  //   ],
  // },
  {
    number: 'Фильм 2',
    alias: 'Comedy',
    image: 'series3.jpg',
    name: 'комедИИант',
    duration: '12 мин',
    description:
      'Программист Никита пытается добиться взаимной симпатии от  коллеги Дианы, рассмешив ее. Но она не отвечает на его ухаживания холодом и никогда не смеётся над его шутками. Тогда он придумывает проект по созданию искусственного интеллекта - комедианта, и приглашает Диану разработчиком в команду. Ведь,если она сама создаст искусственный интеллект, то он точно будет придумывать смешные для неё шутки. Но кажется даже самые инновационные технологии не способны растопить сердце Несмеяны.',
    // fullDescription: ``,
    url: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/1d0203ab-f632-4101-be20-82b83c825a2f/komediiant.ism/manifest',
    // trailer: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    // trailerSmall: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    authors: 'Сценарий: <b>Дима Акатов, Елена Бабенко, Мириам Комиссарова</b><br/>Режиссер: <b>Дима Акатов</b><br/>Оператор: <b>Ильяс Газизов</b><br/>Актеры: <b>Данила Гульков, Илария Лопатина, Камиль Юсупов, Кристина Селезнева, Софья Люминарская</b><br/>Монтаж: <b>Дима Акатов</b>',
    sequence: [
      {
        start: 677.565631,
        end: 796,
        type: 'NextSeries',
      },
    ],
  },
  {
    number: 'Фильм 3',
    alias: 'UnrealIntelligence',
    image: 'series4.jpg',
    name: 'Нереальный интеллект',
    duration: '13 мин',
    description:
      'Программист-самоучка Вова приезжает в Иннополис на отбор в Startup House и случайно наделяет созданный им искусственный интеллект характером провинциального четкого пацана (подростка, который слушает рэп и любит цитаты из пацанских пабликов). Вместе они попытаются обойти серьезных претендентов на победу и пройти отбор.',
    // fullDescription: ``,
    url: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/683b8ea6-6084-4ba2-a4cc-adaf21c1715e/ni.ism/manifest',
    // trailer: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    // trailerSmall: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    authors: 'Сценарий: <b>Юля Завистяева, Мириам Комиссарова</b><br/>Режиссер: <b>Мириам Комиссарова</b><br/>Оператор: <b>Корней Белых</b><br/>Актеры: <b>Алексей Галкин, Глеб Рудик и Полина Малых</b><br/>Монтаж: <b>Мириам Комиссарова</b>',
    sequence: [
      {
        start: 726.414141,
        end: 796,
        type: 'NextSeries',
      },
    ],
  },
  // {
  //   number: 'Фильм 5',
  //   alias: 'Icontact',
  //   image: 'series5.jpg',
  //   name: 'Ай-контакт',
  //   duration: '8 мин',
  //   description:
  //     'Стартаперы-резиденты Иннополиса Ян и Кристина готовят к презентации перед инвесторами ай-трекер - технологию, которая по частоте моргания и скорости передвижения глаз может определить уровень стресса, степень эмоционального выгорания и психомоторные показатели. Происходит технический сбой, и за два часа до выступления героям предстоит починить разработку, сохранив свои отношения. ',
  //   // fullDescription: ``,
  //   url: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/78f4716d-46df-4a2f-8e8f-7f24ae996b63/aicontact.ism/manifest',
  //   // trailer: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
  //   // trailerSmall: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
  //   authors: 'Автор сценария: <b>Анастасия Политова</b><br/>Продюсеры: <b>Валерия Воляндо, Анастасия Политова</b><br/>Режиссёр: <b>Дмитрий Тесля</b><br/>Оператор: <b>Дмитрий Тесля</b><br/>Актеры: <b>Михаил Селиванов, Дарья Катышова, Григорий Козинов</b><br/>Монтажеры: <b>Дмитрий Тесля, Валерия Воляндо</b><br/>Композитор: <b>Олег Воляндо</b>',
  //   sequence: [
  //     {
  //       start: 430.744692,
  //       end: 796,
  //       type: 'NextSeries',
  //     },
  //   ],
  // },
  {
    number: 'Фильм 4',
    alias: 'findInNet',
    image: 'series6.jpg',
    name: '#найти_в_сети',
    duration: '12 мин',
    description:
      'Даниил – молодой программист, работающий на IT-стартап. Он тайно влюблен девушку Лизу, активно ведущую свой блог, в котором рассказывает буквально о каждом своем шаге. Но однажды она пропадает и перестает публиковать новые посты. Молодой человек решает, что с ней что-то случилось и обращается в полицию, но его заявление не принимают. Тогда он решает самостоятельно найти Лизу, используя Интернет – открытые источники и программы, разработкой которых занимается стартап, в котором он работает. С помощью алгоритмов ИИ, нейросетей и моделей машинного обучения Даниилу удается выйти на след девушки, которую, кажется, и правда похитили…',
    // fullDescription: ``,
    url: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/940531f8-a98e-4467-9d24-eaed22799b9b/naitivseti.ism/manifest ',
    // trailer: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    // trailerSmall: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    authors: 'Режиссер: <b>Анна Митафиди</b><br/>Cценарист: <b>Олег Курочкин</b><br/>Оператор и монтажер: <b>Андрей Литвиненко</b><br/>Актеры: <b>Никита Алеферов, Геннадий Фокин, Софья Люминарская, Кристина Селезнева, Вячеслав Волков, Олег Бойко</b><br/>',
    sequence: [
      {
        start: 678.091556,
        end: 796,
        type: 'NextSeries',
      },
    ],
  },
  
  /*{
    number: 'Фильм 7',
    image: 'series7.jpg',
    name: 'Инвайт',
    duration: '15 мин',
    description:
      'Антон работает разработчиком в небольшом стартапе, специализирующемся на кибербезопасности. В один вечер он совершает прорыв в разработке, а несколькими часами позже получает приглашение в бета-версию некой загадочной метавсленной...',
    // fullDescription: ``,
    url: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/a9e90cbb-56fa-48b3-bf07-4fbfa783ebeb/sins-s01-e06-v3.ism/manifest',
    // trailer: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    // trailerSmall: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    authors: 'Авторы сценария и режиссеры: <b>Амина Верещагина, Антон Верещагин</b><br/>Оператор: <b>Андрей Лущекин</b><br/>Монтаж: <b>Амина Верещагина</b><br/>Музыка: <b>Джеймс Максвелл Нордвуд</b><br/>Актеры: <b>Павел Винокуров, Аглая Татарникова, Рафис Нуруллин, Камиль Юсипов</b>',
    sequence: [],
  },*/
  {
    number: 'Фильм 5',
    alias: 'Warning',
    image: 'series8.jpg',
    name: 'Предупреждение',
    duration: '13 мин',
    description:
      'Каждый шестой житель России не успевает получить необходимую медицинскую помощь. Благодаря разработке Марины Зарубиной это можно кардинально изменить. Осталось лишь получить такую нужную поддержку инвесторов.',
    // fullDescription: ``,
    url: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/7d74243b-301e-48d8-a90b-7c700e0d7e31/preduprezdenie.ism/manifest',
    // trailer: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    // trailerSmall: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    authors: 'Автор сценария и режиссёр: <b>Роман Константинов</b><br/>Оператор: <b>Егор Петров</b><br/>Актёры: <b>Дарья Мурзакова, Олег Бойко, Николай Черновиков, Александр Максимов, Павел Зайцев, Роман Молочков</b><br/>Монтаж: <b>Роман Константинов, Егор Петров</b>',
    sequence: [
      {
        start: 687.026183,
        end: 796,
        type: 'NextSeries',
      },
    ],
  },
  /*{
    number: 'Фильм 9',
    image: 'series8.jpg',
    name: '36 вопросов',
    duration: '13 мин',
    description:
      '',
    // fullDescription: ``,
    url: 'https://slcinema-endpoint-slcinema-euno.streaming.media.azure.net/a9e90cbb-56fa-48b3-bf07-4fbfa783ebeb/sins-s01-e06-v3.ism/manifest',
    // trailer: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    // trailerSmall: 'https://screenlifevideoplatform.blob.core.windows.net/poster/sins-s01-e06-trailer.mp4',
    authors: '',
    sequence: [],
  },*/
  
]
