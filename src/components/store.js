import { elementScrollIntoView } from 'seamless-scroll-polyfill'
import { writable, derived } from 'svelte/store'
import items from './items'
import sendMetrik from '../metrika';

const toHHMMSS = (secs) => {
  var sec_num = parseInt(secs, 10)
  var hours = Math.floor(sec_num / 3600)
  var minutes = Math.floor(sec_num / 60) % 60
  var seconds = sec_num % 60

  return [hours, minutes, seconds]
    .map((v) => (v < 10 ? '0' + v : v))
    .filter((v, i) => v !== '00' || i > 0)
    .join(':')
}

export const myPlayer = writable()

export const currentTime = writable(0)

export const duration = writable(0)

export const timeLeft = derived([currentTime, duration], ([$currentTime, $duration]) => {
  if (!$duration) return null
  return toHHMMSS($duration - $currentTime)
})

const searchParams = new URLSearchParams(window.location.search)
const initialSeries = Number(searchParams.get('episode'))
export const activeSeriesIndex = writable(
  initialSeries && !Number.isNaN(initialSeries) && initialSeries <= items.length ? initialSeries - 1 : 0,
)

export const isMobilePlayerVisible = writable(false)

export const toggleMobilePlayerVisible = () => {
  isMobilePlayerVisible.update((state) => {
    const newState = !state
    if (newState) {
      window?.azure?.play()
    } else {
      window?.azure?.pause()
    }
    return newState
  })
}

export const setSeries = (i) => {
  sendMetrik('Innopolis.SliderEpisode', {'Innopolis.SliderEpisodeName':items[i].alias})
  activeSeriesIndex.set(i)
  
  elementScrollIntoView(document.querySelector('#anchor'), { behavior: 'smooth' })
}

export const activeSeries = derived(activeSeriesIndex, ($activeSeriesIndex) => {
  return items[$activeSeriesIndex]
})

export const nextSeries = () => {
  activeSeriesIndex.update((index) => {
    if (index === items.length - 1) {
      return index
    }
    window.azure.autoplay(true)
    return index + 1
  })
}

activeSeries.subscribe((store) => {
  currentTime.set(0)
  if (window.azure) {
    window.azure.src([
      {
        src: store.url,
        type: 'application/vnd.ms-sstr+xml',
      },
    ])
  }
})

export const isPolicyOpen = writable(false)

export const hidePoster = writable(false)
